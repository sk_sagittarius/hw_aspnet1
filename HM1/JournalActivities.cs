﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HM1
{
    public class JournalActivities
    {
        List<Student> students = new List<Student>();
        public void CreateStudent()
        {
            Console.WriteLine("First name: ");
            var firstName = Console.ReadLine();
            Console.WriteLine("Last name: ");
            var lastName = Console.ReadLine();
            Console.WriteLine("Mark: ");
            var mark = int.Parse(Console.ReadLine());

            var student = new Student
            {
                FirstName = firstName,
                LastName = lastName,
                Mark = mark
            };
            students.Add(student);
            
        }
        public void TheBestStudent()
        {
            var maxMark = students.Max(x => x.Mark);
            var bestStudents = students.FindAll(x => x.Mark == maxMark);
            Console.WriteLine("Best students: \n");
            foreach (var student in bestStudents)
            {
                Console.WriteLine(student.FirstName + " " + student.LastName + " - " + student.Mark);

            }
            Console.Read();
            
        }
        public void TheWorstStudent()
        {
            var minMark = students.Min(x => x.Mark);
            var worstStudents = students.FindAll(x => x.Mark == minMark);
            Console.WriteLine("Worst students: \n");
            foreach (var student in worstStudents)
            {
                Console.WriteLine(student.FirstName + " " + student.LastName + " - " + student.Mark);

            }
            Console.Read();
        }
        public void GroupAverageMark()
        {
            var sumMark = students.Sum(x => x.Mark);
            var avrgMark = sumMark / students.Count;
            Console.WriteLine("Group average mark: " + avrgMark);
            Console.Read();
        }
        public void ShowAllStudents() 
        {
            int i = 0;
            Console.WriteLine("List of students:\n");
            foreach (var student in students)
            {
                Console.WriteLine(++i + ". " + student.FirstName + " " + student.LastName + " - " + student.Mark);
            }
            Console.WriteLine("\nPress Enter to return to Menu");
            Console.Read();
            
        }
    }
}
