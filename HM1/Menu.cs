﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace HM1
{
    public class Menu
    {
        JournalActivities journalActivities = new JournalActivities();
        //public void TextMenu()
        //{
        //    Console.WriteLine("1. Create student\n2. The best student\n3. The worst student\n4. " +
        //            "Group average mark\n5. Show all students\n6. Exit");
        //    var key = Console.ReadLine();
        //    ShowMenu(key);
        //}
        public void ShowMenu()
        {
            while (true)
            {
                Console.Clear();
                Console.WriteLine("1. Create student\n2. The best student\n3. The worst student\n4. " +
                    "Group average mark\n5. Show all students\n6. Exit");
                var key = Console.ReadLine();

                switch (key)
                {
                    //case 0: Console.WriteLine("1. Create student/n2. The best student/n3. The worst student/n4. " +
                    //    "Group average mark/n5. Show all students/n 6. Exit");
                    //    break;
                    case "1":
                        journalActivities.CreateStudent();
                        break;
                    case "2":
                        journalActivities.TheBestStudent();
                        break;
                    case "3":
                        journalActivities.TheWorstStudent();
                        break;
                    case "4":
                        journalActivities.GroupAverageMark();
                        break;
                    case "5":
                        journalActivities.ShowAllStudents();

                        break;
                    case "6":
                        Environment.Exit(0);
                        break;
                }  
            }
            
        }
    }
}
